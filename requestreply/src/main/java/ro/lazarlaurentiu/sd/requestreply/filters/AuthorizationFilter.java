package ro.lazarlaurentiu.sd.requestreply.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ro.lazarlaurentiu.sd.requestreply.entities.User;
import ro.lazarlaurentiu.sd.requestreply.entities.User.Roles;

/**
 * Servlet Filter implementation class AuthorizationFilter
 */
public class AuthorizationFilter implements Filter {

	private static final String LOGGED_IN_USER = "loggedInUser";

	/**
	 * Default constructor.
	 */
	public AuthorizationFilter() {
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// Not implemented
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// Not implemented
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		User user = (User) httpRequest.getSession().getAttribute(LOGGED_IN_USER);
		if (user == null) {
			// The user is not logged in
			httpResponse.sendRedirect("/requestreplay");
			return;
		}

		String url = httpRequest.getRequestURI();
		if (url.startsWith("/requestreplay/flights/manage") && user.getRole() != Roles.ADMIN) {
			// The user is not authorized
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/authentication");
			requestDispatcher.forward(request, response);
			return;
		}

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

}
