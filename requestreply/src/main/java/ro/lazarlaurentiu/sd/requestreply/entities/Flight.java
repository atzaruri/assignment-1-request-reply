package ro.lazarlaurentiu.sd.requestreply.entities;

import java.util.Date;

/**
 * @author Lazar Laurentiu
 *
 *         Entity class for a Flight.
 */
public class Flight {

	private int flightNumber;
	private String airplaneType;
	private City departureCity;
	private Date departureTime;
	private City arrivalCity;
	private Date arrivalTime;

	public Flight() {
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public City getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public City getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	@Override
	public boolean equals(Object flight) {
		if (flight instanceof Flight) {
			return ((Flight) flight).getFlightNumber() == this.flightNumber;
		}
		return false;
	}

}
