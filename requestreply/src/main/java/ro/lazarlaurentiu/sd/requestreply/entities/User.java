package ro.lazarlaurentiu.sd.requestreply.entities;

/**
 * @author Lazar Laurentiu
 *
 *         Entity class for an User.
 */
public class User {

	public enum Roles {
		ADMIN, REGULAR_USER
	};

	private String username;
	private String password;
	private String name;
	private Roles role;

	public User() {
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return this.username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return this.password;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public Roles getRole() {
		return this.role;
	}
}
