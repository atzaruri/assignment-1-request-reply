package ro.lazarlaurentiu.sd.requestreply.utils;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import ro.lazarlaurentiu.sd.requestreply.entities.GoogleTimeZoneApiResponse;

/**
 * Parser for a response from Google Time Zone API.
 * 
 * @author Laurentiu Lazar
 */
public class GoogleTimeZoneApiJsonResponseParser {

	private static long MILLIS_IN_SECOND = 1000;
	private static long CURRENT_TIME_ZONE_OFFSET = Calendar.getInstance().getTimeZone().getRawOffset()
			/ MILLIS_IN_SECOND;

	private static final Log LOGGER = LogFactory.getLog(GoogleTimeZoneApiJsonResponseParser.class);

	/**
	 * Computes the time represented in the Google Time Zone API Json Response.
	 * 
	 * @param queryTimestampInSeconds
	 *            The timestamp (in seconds) used to query Google Time Zone API.
	 * @param jsonResponse
	 *            The json response from Google Time Zone API.
	 * @return a Date Object containing the the time represented in the Google
	 *         Time Zone Response. If the response status is not 'Success' or if
	 *         there are parsing errors, the method returns null.
	 */
	public static Date getTimeFromGoogleApiJsonResponse(long queryTimestampInSeconds, String jsonResponse) {
		GoogleTimeZoneApiResponse apiResponse = null;
		Gson jsonParser = new Gson();

		try {
			apiResponse = jsonParser.fromJson(jsonResponse, GoogleTimeZoneApiResponse.class);
		} catch (JsonSyntaxException e) {
			LOGGER.error("Exception in GoogleTimeZoneApiResponse!", e);
			return null;
		}

		if (!apiResponse.isSuccessful()) {
			LOGGER.error("Google Time Zone API response not successful! Message: " + apiResponse.getErrorMessage());
			return null;
		}

		long newTimestampInSeconds = queryTimestampInSeconds + apiResponse.getDstOffset() + apiResponse.getRawOffset()
				- CURRENT_TIME_ZONE_OFFSET;
		return new Date(newTimestampInSeconds * MILLIS_IN_SECOND);
	}

}
