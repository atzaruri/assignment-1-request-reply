package ro.lazarlaurentiu.sd.requestreply.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ro.lazarlaurentiu.sd.requestreply.entities.Flight;

/**
 * @author Laurentiu Lazar
 * 
 *         Data Access Object for Flights.
 */
public class FlightsDao {

	private SessionFactory factory;

	private static final Log LOGGER = LogFactory.getLog(FlightsDao.class);

	public FlightsDao(SessionFactory factory) {
		this.factory = factory;
	}

	public boolean addFlight(Flight flight) {
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(flight);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Exception in addFlight()!", e);
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	public boolean editFlight(Flight flight) {
		Session session = factory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.merge(flight);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
				return false;
			}
			LOGGER.error("Exception in editFlight()!", e);
		} finally {
			session.close();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public Flight getFlight(int flightNumber) {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<Flight> flights = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE flightNumber = :flightNumber");
			query.setParameter("flightNumber", flightNumber);
			flights = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: getFlight", e);
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> getAllFlights() {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<Flight> flights = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Flight");
			flights = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: getAllFlights", e);
		} finally {
			session.close();
		}
		return flights;
	}

	@SuppressWarnings("unchecked")
	public boolean deleteFlight(int flightNumber) {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<Flight> flights;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE flightNumber=:flightNumber");
			query.setParameter("flightNumber", flightNumber);
			flights = query.list();

			if (flights.size() < 1) {
				return false;
			}
			Flight flight = flights.get(0);
			session.delete(flight);
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: deleteFlight", e);
		} finally {
			if (transaction != null) {
				transaction.commit();
			}
			session.close();
		}
		return true;
	}
}
