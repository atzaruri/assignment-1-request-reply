package ro.lazarlaurentiu.sd.requestreply.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ro.lazarlaurentiu.sd.requestreply.entities.City;

/**
 * @author Laurentiu Lazar
 * 
 *         Data Access Object for Cities.
 */
public class CitiesDao {

	private static final Log LOGGER = LogFactory.getLog(CitiesDao.class);

	private SessionFactory factory;

	public CitiesDao(SessionFactory factory) {
		this.factory = factory;
	}

	@SuppressWarnings("unchecked")
	public City getCity(int cityId) {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<City> cities = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE id = :id");
			query.setParameter("id", cityId);
			cities = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: getCity", e);
		} finally {
			session.close();
		}
		return cities != null && !cities.isEmpty() ? cities.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<City> getAllCities() {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<City> cities = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM City ");
			cities = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: getAllCities", e);
		} finally {
			session.close();
		}
		return cities;
	}
}
