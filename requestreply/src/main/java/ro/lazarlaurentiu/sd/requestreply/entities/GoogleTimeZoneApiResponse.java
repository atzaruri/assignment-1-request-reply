package ro.lazarlaurentiu.sd.requestreply.entities;

/**
 * Entity class for a Google Time Zone API Response.
 * 
 * @author Lazar Laurentiu
 */
public class GoogleTimeZoneApiResponse {

	/**
	 * Offset for Daylight Savings Time in seconds.
	 */
	private long dstOffset;
	/**
	 * Raw offset from UTC in seconds.
	 */
	private long rawOffset;
	/**
	 * Response status. "OK" when the query is successful.
	 */
	private String status;
	/**
	 * Short Name of the Time Zone.
	 */
	private String timeZoneId;
	/**
	 * Long Name of the Time Zone.
	 */
	private String timeZoneName;
	/**
	 * Error message, in case that the status != "OK".
	 */
	private String errorMessage;

	/**
	 * Response OK status.
	 */
	private static final String OK_STATUS = "OK";

	public GoogleTimeZoneApiResponse() {
	}

	public long getDstOffset() {
		return dstOffset;
	}

	public void setDstOffset(long dstOffset) {
		this.dstOffset = dstOffset;
	}

	public long getRawOffset() {
		return rawOffset;
	}

	public void setRawOffset(long rawOffset) {
		this.rawOffset = rawOffset;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(String timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getTimeZoneName() {
		return timeZoneName;
	}

	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isSuccessful() {
		return status.equals(OK_STATUS);
	}

}
