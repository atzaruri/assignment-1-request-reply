package ro.lazarlaurentiu.sd.requestreply.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import ro.lazarlaurentiu.sd.requestreply.dao.UsersDao;
import ro.lazarlaurentiu.sd.requestreply.entities.User;
import ro.lazarlaurentiu.sd.requestreply.entities.User.Roles;

/**
 * @author Lazar Laurentiu
 * 
 *         Servlet implementation class AuthenticationServlet.
 */
public class AuthenticationServlet extends HttpServlet {

	private UsersDao usersDao;

	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String INVALID_CREDENTIALS = "invalidCredentials";
	private static final String LOGGED_IN_USER = "loggedInUser";

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthenticationServlet() {
		super();
		usersDao = new UsersDao(new Configuration().configure().buildSessionFactory());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute(LOGGED_IN_USER);
		if (user == null) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		redirectUser(request, response, user);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");
		String username = request.getParameter(USERNAME);

		if (username != null) {
			doLogin(request, response);
		} else {
			doLogout(request, response);
		}
	}

	/**
	 * Try login of an user.
	 * 
	 * @param request
	 *            HttpRequest
	 * @param response
	 *            HttpResponse
	 */
	private void doLogin(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter(USERNAME);
		String password = request.getParameter(PASSWORD);

		User user = usersDao.getUser(username);

		if (user == null || user.getPassword().compareTo(password) != 0) {
			request.setAttribute(INVALID_CREDENTIALS, true);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
			requestDispatcher.forward(request, response);
			return;
		}
		request.getSession().setAttribute(LOGGED_IN_USER, user);
		redirectUser(request, response, user);
	}

	/**
	 * Redirect an user based on his role.
	 * 
	 * @param request
	 *            HttpRequest.
	 * @param response
	 *            HttpResponse.
	 * @param user
	 *            The user.
	 */
	private void redirectUser(HttpServletRequest request, HttpServletResponse response, User user)
			throws ServletException, IOException {
		if (user.getRole() == Roles.ADMIN) {
			response.sendRedirect("/requestreplay/flights/manage");
		} else {
			response.sendRedirect("/requestreplay/flights/display");
		}
	}

	/**
	 * Do logout of an user.
	 * 
	 * @param request
	 *            HttpRequest
	 * @param response
	 *            HttpResponse
	 */
	private void doLogout(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getSession().removeAttribute(LOGGED_IN_USER);
		response.sendRedirect("");
	}
}
