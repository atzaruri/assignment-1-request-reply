package ro.lazarlaurentiu.sd.requestreply.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Configuration;

import ro.lazarlaurentiu.sd.requestreply.dao.CitiesDao;
import ro.lazarlaurentiu.sd.requestreply.dao.FlightsDao;
import ro.lazarlaurentiu.sd.requestreply.entities.City;
import ro.lazarlaurentiu.sd.requestreply.entities.Flight;

/**
 * @author Lazar Laurentiu
 * 
 *         Servlet implementation class EditFlightsServlet.
 */
public class ManageFlightsServlet extends HttpServlet {

	private FlightsDao flightsDao;
	private CitiesDao citiesDao;

	private static final String FLIGHTS = "flights";
	private static final String FLIGHT_NUMBER = "flightNumber";
	private static final String CITIES = "cities";
	private static final String ADD_TITLE = "Add new flight";
	private static final String EDIT_TITLE = "Edit flight";
	private static final String TITLE = "title";
	private static final String ERROR = "error";
	private static final String FLIGHT_TO_BE_EDITED = "flightToBeEdited";
	private static final String ACTION = "action";

	private static final String PARAMETER_FLIGHT_NUMMBER = FLIGHT_NUMBER;
	private static final String PARAMETER_AIRPLANE_TYPE = "airplaneType";
	private static final String PARAMETER_DEPARTURE_CITY = "departureCity";
	private static final String PARAMETER_DEPARTURE_TIME = "departureTime";
	private static final String PARAMETER_ARRIVAL_CITY = "arrivalCity";
	private static final String PARAMETER_ARRIVAL_TIME = "arrivalTime";

	private static final String ERROR_EMPTY_FIELDS = "Please fill in all fields!";
	private static final String ERROR_DEPARTURE_ARRIVAL_CITY_MATCH = "Departure city and arrival city cannot be the same one!";
	private static final String ERROR_INVALID_DEPARTURE_TIME = "Invalid departure time!";
	private static final String ERROR_INVALID_ARRIVAL_TIME = "Invalid arrival time!";

	private static final String MANAGE_REQUEST = "/requestreplay/flights/manage";
	private static final String ADD_REQUEST = "/requestreplay/flights/manage/add";
	private static final String EDIT_REQUEST = "/requestreplay/flights/manage/edit";
	private static final String DELETE_REQUEST = "/requestreplay/flights/manage/delete";

	private static final long serialVersionUID = 1L;

	private static final Log LOGGER = LogFactory.getLog(ManageFlightsServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManageFlightsServlet() {
		super();

		flightsDao = new FlightsDao(new Configuration().configure().buildSessionFactory());
		citiesDao = new CitiesDao(new Configuration().configure().buildSessionFactory());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getRequestURI();

		if (url.startsWith(ADD_REQUEST)) {
			List<City> cities = citiesDao.getAllCities();

			request.setAttribute(CITIES, cities.toArray(new City[0]));
			request.setAttribute(TITLE, ADD_TITLE);
			request.setAttribute(ACTION, ADD_REQUEST);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("addEditFlight.jsp");
			requestDispatcher.forward(request, response);

			return;
		}

		if (url.startsWith(EDIT_REQUEST)) {
			int flightNumber;
			try {
				flightNumber = Integer.parseInt((String) request.getParameter(FLIGHT_NUMBER));
			} catch (NumberFormatException e) {
				LOGGER.error("Method: doGet", e);
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			Flight flight = flightsDao.getFlight(flightNumber);
			if (flight == null) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}

			List<City> cities = citiesDao.getAllCities();

			request.setAttribute(CITIES, cities.toArray(new City[0]));
			request.setAttribute(TITLE, EDIT_TITLE);
			request.setAttribute(ACTION, EDIT_REQUEST);
			request.setAttribute(FLIGHT_TO_BE_EDITED, flight);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("addEditFlight.jsp");
			requestDispatcher.forward(request, response);

			return;
		}

		if (url.startsWith(MANAGE_REQUEST)) {
			List<Flight> flights = flightsDao.getAllFlights();

			request.setAttribute(FLIGHTS, flights.toArray(new Flight[0]));

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("manageFlights.jsp");
			requestDispatcher.forward(request, response);

			return;
		}

		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getRequestURI();

		if (url.startsWith(ADD_REQUEST)) {
			handleAddRequest(request, response);
			return;
		}

		if (url.startsWith(EDIT_REQUEST)) {
			handleEditRequest(request, response);
			return;
		}

		if (url.endsWith(DELETE_REQUEST)) {
			int flightNumber = Integer.parseInt(request.getParameter(FLIGHT_NUMBER));
			if (flightsDao.deleteFlight(flightNumber)) {
				response.sendRedirect("/requestreplay/flights/manage");
			}

			return;
		}

		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}

	private void handleAddRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Flight flight = parseParameters(request, response);

		if (flight == null) {
			return;
		}

		flightsDao.addFlight(flight);
		response.sendRedirect(MANAGE_REQUEST);
	}

	private void handleEditRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Flight flight = parseParameters(request, response);

		if (flight == null) {
			return;
		}

		flightsDao.editFlight(flight);
		response.sendRedirect(MANAGE_REQUEST);
	}

	private Flight parseParameters(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int flightNumber = -1;
		if (request.getParameter(PARAMETER_FLIGHT_NUMMBER) != null) {
			flightNumber = Integer.parseInt((String) request.getParameter(PARAMETER_FLIGHT_NUMMBER));
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		String airplaneType = (String) request.getParameter(PARAMETER_AIRPLANE_TYPE);
		if (airplaneType == null || airplaneType.trim().isEmpty()) {
			sendError(request, response, ERROR_EMPTY_FIELDS);
			return null;
		}

		String[] tokens = ((String) request.getParameter(PARAMETER_DEPARTURE_CITY)).split(" ");
		int departureCityId = Integer.parseInt(tokens[0]);

		Date departureTime;
		try {

			departureTime = dateFormat.parse((String) request.getParameter(PARAMETER_DEPARTURE_TIME));
		} catch (ParseException e) {
			LOGGER.error("Method: parseParameters", e);
			sendError(request, response, ERROR_INVALID_DEPARTURE_TIME);
			return null;
		}

		tokens = ((String) request.getParameter(PARAMETER_ARRIVAL_CITY)).split(" ");
		int arrivalCityId = Integer.parseInt(tokens[0]);

		Date arrivalTime;
		try {
			arrivalTime = dateFormat.parse((String) request.getParameter(PARAMETER_ARRIVAL_TIME));

		} catch (ParseException e) {
			LOGGER.error("Method: parseParameters", e);
			sendError(request, response, ERROR_INVALID_ARRIVAL_TIME);
			return null;
		}

		if (departureCityId == arrivalCityId) {
			sendError(request, response, ERROR_DEPARTURE_ARRIVAL_CITY_MATCH);
			return null;
		}

		Flight flight = new Flight();
		if (flightNumber != -1) {
			flight.setFlightNumber(flightNumber);
		}
		City departureCity = new City();
		departureCity.setId(departureCityId);
		City arrivalCity = new City();
		arrivalCity.setId(arrivalCityId);
		flight.setAirplaneType(airplaneType);
		flight.setDepartureCity(departureCity);
		flight.setDepartureTime(departureTime);
		flight.setArrivalCity(arrivalCity);
		flight.setArrivalTime(arrivalTime);

		return flight;
	}

	private void sendError(HttpServletRequest request, HttpServletResponse response, String message)
			throws ServletException, IOException {
		request.setAttribute(ERROR, message);
		doGet(request, response);
	}

}
