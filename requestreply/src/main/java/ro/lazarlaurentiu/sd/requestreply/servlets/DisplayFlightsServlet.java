package ro.lazarlaurentiu.sd.requestreply.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import ro.lazarlaurentiu.sd.requestreply.dao.FlightsDao;
import ro.lazarlaurentiu.sd.requestreply.entities.Flight;

/**
 * @author Lazar Laurentiu
 * 
 *         Servlet implementation class DisplayFlightsServlet
 */
public class DisplayFlightsServlet extends HttpServlet {

	private FlightsDao flightsDao;

	private static final long serialVersionUID = 1L;
	private static final String FLIGHTS = "flights";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DisplayFlightsServlet() {
		super();
		flightsDao = new FlightsDao(new Configuration().configure().buildSessionFactory());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Flight> flights = flightsDao.getAllFlights();

		request.setAttribute(FLIGHTS, flights.toArray(new Flight[0]));

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("displayFlights.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
