package ro.lazarlaurentiu.sd.requestreply.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.hibernate.cfg.Configuration;

import ro.lazarlaurentiu.sd.requestreply.dao.CitiesDao;
import ro.lazarlaurentiu.sd.requestreply.entities.City;
import ro.lazarlaurentiu.sd.requestreply.utils.GoogleTimeZoneApiJsonResponseParser;

/**
 * Servlet implementation class LocalTimeServlet
 * 
 * @author Lazar Laurentiu
 */
public class LocalTimeServlet extends HttpServlet {

	private CitiesDao citiesDao;

	private static final String GOOGLE_API_URL = "https://maps.googleapis.com/maps/api/timezone/json?location=%f,%f&timestamp=%d&key=%s";
	private static final String GOOGLE_API_KEY = "AIzaSyC0kA2u1RK7TNDGcQjLoQazD2YxMMS5NdU";
	private static final String CITY_ID = "cityId";
	private static final String SUCCESS_MESSAGE = "Local time in <b>%s</b> is <b>%s</b>";
	private static final String ERROR_MESSAGE = "Could not fetch local time for <b>%s</b>";
	private static final String LOCAL_TIME = "localTime";;

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
	private static final Log LOGGER = LogFactory.getLog(LocalTimeServlet.class);

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LocalTimeServlet() {
		super();

		citiesDao = new CitiesDao(new Configuration().configure().buildSessionFactory());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int cityId = Integer.parseInt(request.getParameter(CITY_ID));
		City city = citiesDao.getCity(cityId);

		if (city == null) {
			LOGGER.error("Invalid city ID!");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		long currentTimestamp = System.currentTimeMillis() / 1000;

		String url = String.format(GOOGLE_API_URL, city.getLatitude(), city.getLongitude(), currentTimestamp,
				GOOGLE_API_KEY);

		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(url);
		CloseableHttpResponse googleApiResponse = null;

		try {
			googleApiResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = googleApiResponse.getEntity();

			if (httpEntity == null || httpEntity.getContent() == null) {
				sendResponse(request, response, getErrorMessage(city));
				return;
			}

			String jsonResponse = IOUtils.toString(httpEntity.getContent(),
					org.apache.commons.lang3.CharEncoding.UTF_8);

			Date localTime = GoogleTimeZoneApiJsonResponseParser.getTimeFromGoogleApiJsonResponse(currentTimestamp,
					jsonResponse);

			if (localTime == null) {
				LOGGER.error("GoogleTimeZoneApiJsonResponseParser could not compute local time!");
				sendResponse(request, response, getErrorMessage(city));
				return;
			}

			// Success
			sendResponse(request, response, getSuccessMessage(city, localTime));

			EntityUtils.consume(httpEntity);
		} catch (IOException e) {
			LOGGER.error("IOException in LocalTimeServlet", e);
			sendResponse(request, response, getErrorMessage(city));
			return;
		} finally {
			if (googleApiResponse != null) {
				googleApiResponse.close();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Format response message for success.
	 */
	private String getSuccessMessage(City city, Date time) {
		return String.format(SUCCESS_MESSAGE, city.getName(), dateFormat.format(time));
	}

	/**
	 * Format response message for error.
	 */
	private String getErrorMessage(City city) {
		return String.format(ERROR_MESSAGE, city.getName());
	}

	/**
	 * Send response.
	 */
	private void sendResponse(HttpServletRequest request, HttpServletResponse response, String responseMessage)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/flights/display");
		request.setAttribute(LOCAL_TIME, responseMessage);
		requestDispatcher.forward(request, response);
	}
}
