<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Display Flights</title>

<!-- Bootstrap -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet">
</head>

<link rel="stylesheet" href="../login.css" />
<body>
	<nav class="navbar navbar-default "></nav>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%
				String userName = ((ro.lazarlaurentiu.sd.requestreply.entities.User) session.getAttribute("loggedInUser"))
						.getName();
			%>
			<div class="navbar-brand navbar-right">
				<form action="/requestreplay/authentication" method="post">
					<button type="submit" style="background: none; border: none"
						title="Logout">
						Logged in as:
						<%=userName%></button>
				</form>
			</div>
			<div class="navbar-header">
				<div class="navbar-brand" align="center">
					<a href="/requestreplay/authentication">Display flights</a>
				</div>
			</div>
		</div>
	</nav>

	<%
		String localTime = (String) request.getAttribute("localTime");
		if (localTime != null) {
	%>
	<div class="text-center" style="padding: 10px 0">
		<div class=logo>
			<%=localTime%>
		</div>
	</div>
	<%
		}
	%>

	<div class="container">
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="text-align: center">Flight Number</th>
					<th style="text-align: center">Airplane Type</th>
					<th style="text-align: center">Departure City</th>
					<th style="text-align: center">Departure Time</th>
					<th style="text-align: center">Arrival City</th>
					<th style="text-align: center">Arrival Time</th>
				</tr>
			</thead>
			<tbody>
				<%
					SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
					ro.lazarlaurentiu.sd.requestreply.entities.Flight[] flights = (ro.lazarlaurentiu.sd.requestreply.entities.Flight[]) request
							.getAttribute("flights");
					for (ro.lazarlaurentiu.sd.requestreply.entities.Flight it : flights) {
						String departureTime = dateFormatter.format(it.getDepartureTime());
						String arrivalTime = dateFormatter.format(it.getArrivalTime());
				%>
				<tr>
					<td align="center" style="font-size: 17px"><%=it.getFlightNumber()%></td>
					<td align="center" style="font-size: 17px"><%=it.getAirplaneType()%></td>
					<td align="center" style="font-size: 17px"><a
						href="/requestreplay/flights/localtime?cityId=<%=it.getDepartureCity().getId()%>">
							<%=it.getDepartureCity().getName()%>
					</a></td>
					<td align="center" style="font-size: 17px"><%=departureTime%></td>
					<td align="center" style="font-size: 17px"><a
						href="/requestreplay/flights/localtime?cityId=<%=it.getArrivalCity().getId()%>">
							<%=it.getArrivalCity().getName()%>
					</a></td>
					<td align="center" style="font-size: 17px"><%=arrivalTime%></td>
				</tr>
				<%
					}
				%>
			
		</table>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</body>
</html>