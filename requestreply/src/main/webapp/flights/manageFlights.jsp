<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Display Flights</title>

<!-- Bootstrap -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet">
</head>

<link rel="stylesheet" href="../login.css" />
<body>
	<nav class="navbar navbar-default "></nav>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%
				String userName = ((ro.lazarlaurentiu.sd.requestreply.entities.User) session.getAttribute("loggedInUser"))
						.getName();
			%>
			<div class="navbar-brand navbar-right">
				<form action="/requestreplay/authentication" method="post">
					<button type="submit" style="background: none; border: none"
						title="Logout">
						Logged in as:
						<%=userName%>
						(A)
					</button>
				</form>
			</div>
			<div class="navbar-header">
				<div class="navbar-brand" align="center">
					<a href="/requestreplay/authentication">Manage flights</a>
				</div>
				<div class="navbar-brand" align="center">
					<form action="/requestreplay/flights/manage/add" method="get">
						<button type="submit" class="btn btn-default btn-lg">
							<span class="glyphicon glyphicon-plus"></span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</nav>

	<div class="container">
		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th style="text-align: center">Flight Number</th>
					<th style="text-align: center">Airplane Type</th>
					<th style="text-align: center">Departure City</th>
					<th style="text-align: center">Departure Time</th>
					<th style="text-align: center">Arrival City</th>
					<th style="text-align: center">Arrival Time</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<%
					SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
					ro.lazarlaurentiu.sd.requestreply.entities.Flight[] flights = (ro.lazarlaurentiu.sd.requestreply.entities.Flight[]) request
							.getAttribute("flights");
					for (ro.lazarlaurentiu.sd.requestreply.entities.Flight it : flights) {
						String departureTime = dateFormatter.format(it.getDepartureTime());
						String arrivalTime = dateFormatter.format(it.getArrivalTime());
				%>
				<tr>
					<td align="center" style="font-size: 17px"><%=it.getFlightNumber()%></td>
					<td align="center" style="font-size: 17px"><%=it.getAirplaneType()%></td>
					<td align="center" style="font-size: 17px"><%=it.getDepartureCity().getName()%></td>
					<td align="center" style="font-size: 17px"><%=departureTime%></td>
					<td align="center" style="font-size: 17px"><%=it.getArrivalCity().getName()%></td>
					<td align="center" style="font-size: 17px"><%=arrivalTime%></td>
					<td align="center">
						<form action="/requestreplay/flights/manage/edit" method="get"
							style="padding-left: 10px; padding-right: 10px;">
							<input type="hidden" name="flightNumber"
								value="<%=it.getFlightNumber()%>" />
							<button type="submit"
								style="background: transparent; border: none; width: 30px; height: 25px; float: left;">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</button>
						</form>
						<form action="/requestreplay/flights/manage/delete" method="post"
							style="padding-left: 10px; padding-right: 10px;">
							<input type="hidden" name="flightNumber"
								value="<%=it.getFlightNumber()%>" />
							<button type="submit"
								style="background: none; border: none; width: 30px; height: 25px;">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</button>
						</form>
					</td>
				</tr>
				<%
					}
				%>
			
		</table>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</body>
</html>