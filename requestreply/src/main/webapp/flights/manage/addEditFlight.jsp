<!DOCTYPE html>
<%@page import="ro.lazarlaurentiu.sd.requestreply.entities.Flight"%>
<%@page import="java.text.SimpleDateFormat"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Display Flights</title>

<!-- Bootstrap -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"
	rel="stylesheet">

<link rel="stylesheet" href="../../login.css" />
</head>
<body>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script
		src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
	<script
		src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

	<nav class="navbar navbar-default "></nav>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%
				String userName = ((ro.lazarlaurentiu.sd.requestreply.entities.User) session.getAttribute("loggedInUser"))
						.getName();
			%>
			<div class="navbar-brand navbar-right">
				<form action="/requestreplay/authentication" method="post">
					<button type="submit" style="background: none; border: none"
						title="Logout">
						Logged in as:
						<%=userName%>
						(A)
					</button>
				</form>
			</div>
			<div class="navbar-header">
				<div class="navbar-brand" align="center">
					<a href="/requestreplay/authentication">Manage flights</a>
				</div>
			</div>
		</div>
	</nav>
	<%  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    	String airplaneType = null, departureTime = null, arrivalTime = null;
    	Integer flightNumber = null, departureCityId = null, arrivalCityId = null;
		
    	Flight flight = (Flight) request.getAttribute("flightToBeEdited");
		
    	if (flight != null) {
    		flightNumber = flight.getFlightNumber();
    	} else {
    		if (request.getParameter("flightNumber") != null) {
    			flightNumber = Integer.parseInt(request.getParameter("flightNumber")); 
    		}
    	}
	    airplaneType = flight != null ? flight.getAirplaneType() : request.getParameter("airplaneType");
	    if (flight != null) {
	    	departureCityId = flight.getDepartureCity().getId();
	    } else {
	    	if (request.getParameter("departureCity") != null) {
	    		departureCityId = Integer.parseInt(request.getParameter("departureCity").split(" ")[0]);
	    	}
	    }
	    departureTime = flight != null ? dateFormat.format(flight.getDepartureTime()) : request.getParameter("departureTime");
	    if (flight != null) {
	    	arrivalCityId = flight.getArrivalCity().getId();
	    } else {
	    	if (request.getParameter("arrivalCity") != null) {
	    		arrivalCityId = Integer.parseInt(request.getParameter("arrivalCity").split(" ")[0]);
	    	}
	    }
	    arrivalTime = flight != null ? dateFormat.format(flight.getArrivalTime()) : request.getParameter("arrivalTime");
	    
	%>
	<div class="text-center" style="padding: 50px 0">
		<div class="logo"><%=request.getAttribute("title")%></div>
		<div class="container">
			<form class="form-horizontal" role="form"
				action=<%=request.getAttribute("action") %>
				method="post">
				<% if (flightNumber != null) { %>
					<input class="text" id="flightNumber" type="text" name="flightNumber" hidden="true" value="<%=flightNumber%>">
				<% } %>
				<div class="form-group">
					<label class="col-sm-5 control-label">Airplane Type</label>
					<div class="col-sm-3">
						<input class="form-control" id="airplaneType" type="text"
							name="airplaneType" <% if (airplaneType != null) { %> value="<%=airplaneType %>" <%} %>>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Departure city</label>
					<div class="col-sm-3">
						<select class="form-control" id="departureCity"
							name="departureCity">
							<%
								ro.lazarlaurentiu.sd.requestreply.entities.City[] cities = (ro.lazarlaurentiu.sd.requestreply.entities.City[]) request
										.getAttribute("cities");
								for (ro.lazarlaurentiu.sd.requestreply.entities.City it : cities) {
							%>
							<option <% if (departureCityId != null && departureCityId == it.getId()) {%> selected="selected"<% } %>><%=it.getId() + " " + it.getName()%></option>
							<%
								}
							%>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Departure time</label>
					<div class="container">
					    <div class="row">
					        <div class='col-sm-3'>
					            <div class="form-group">
					                <div class='input-group date' id='departureDateTimePicker'>
					                    <input type='text' class="form-control" id='departureTime' name='departureTime'
					                    <% if (departureTime != null) { %> value="<%=departureTime %>" <%} %>/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#departureDateTimePicker').datetimepicker({
					                	format: 'DD-MM-YYYY HH:mm'
					                });
					            });
					        </script>
					    </div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Arrival city</label>
					<div class="col-sm-3">
						<select class="form-control" id="arrivalCity" name="arrivalCity">
							<%
								for (ro.lazarlaurentiu.sd.requestreply.entities.City it : cities) {
							%>
							<option <% if (arrivalCityId != null && arrivalCityId == it.getId()) {%> selected="selected" <%} %>><%=it.getId() + " " + it.getName() %></option>
							<%
								}
							%>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Arrival time</label>
					  <div class="container">
					    <div class="row">
					        <div class='col-sm-3'>
					            <div class="form-group">
					                <div class='input-group date' id='arrivalDateTimePicker'>
					                    <input type='text' class="form-control" id='arrivalTime' name='arrivalTime'
					                    <% if (arrivalTime != null) { %> value="<%=arrivalTime %>" <%} %>/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#arrivalDateTimePicker').datetimepicker({
					                	format: 'DD-MM-YYYY HH:mm'
					                });
					            });
					        </script>
					    </div>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-default">Save</button>
					<a href="/requestreplay/flights/manage">
  						<input type="button" class="btn btn-default" value="Cancel" />
					</a>
				</div>
			</form>
		</div>

		<%
			String error = (String) request.getAttribute("error");
			if (error != null) {
		%>
		<div class="alert alert-error">
			<strong style="color: red"><%=error%></strong>
		</div>
		<%
			}
		%>
	</div>
</body>
</html>