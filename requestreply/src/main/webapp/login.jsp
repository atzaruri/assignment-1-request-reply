<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Login</title>

<!-- Bootstrap -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet">
<!-- All the files that are required -->
<link rel='stylesheet' type='text/css' href="login.css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Varela+Round'
	rel='stylesheet' type='text/css'>
<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1" />
</head>
<body>
	<!-- Where all the magic happens -->
	<!-- LOGIN FORM -->
	<div class="text-center" style="padding: 50px 0">
		<div class="logo">login</div>
		<!-- Main Form -->
		<div class="login-form-1">
			<form id="login-form" class="text-left" method="post"
				action="/requestreplay/authentication" onsubmit='encryptPasswordMD5()'>
				<div class="login-form-main-message"></div>
				<div class="main-login-form">
					<div class="login-group">
						<div class="form-group">
							<label for="username" class="sr-only">Username</label> 
								<input type="text" class="form-control" id="username" name="username"
								placeholder="username" autofocus="autofocus">
						</div>
						<div class="form-group">
							<label for="password" class="sr-only">Password</label> 
							<input type="password" class="form-control" id="password"
								name="password" placeholder="password">
						</div>
					</div>
					<button type="submit" class="login-button">
						<i class="fa fa-chevron-right"></i>
					</button>
				</div>
			</form>
		</div>
		<%
			if (request.getAttribute("invalidCredentials") != null) {
				Boolean invalidCredentials = (Boolean) request.getAttribute("invalidCredentials");
				if (invalidCredentials) {
		%>
		<div class="logo">Username or password is incorrect!</div>
		<%
				}
			}
		%>
		<!-- end:Main Form -->
	</div>
	
	<script>
		function encryptPasswordMD5() {
			if(document.getElementById('password').value) {
				document.getElementById('password').value = CryptoJS.MD5(document.getElementById('password').value);
				}
			}
	</script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
	<script src="login.js"></script>
</body>
</html>