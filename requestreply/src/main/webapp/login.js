(function($) {
	"use strict";

	// Login Form
	// ----------------------------------------------
	// Validation
	$("#login-form").validate({
		rules : {
			username : "required",
			password : "required",
		},
		errorClass : "form-invalid"
	});

	// Loading
	// ----------------------------------------------
	function remove_loading($form) {
		$form.find('[type=submit]').removeClass('error success');
		$form.find('.login-form-main-message')
				.removeClass('show error success').html('');
	}

	function form_loading($form) {
		$form.find('[type=submit]').addClass('clicked').html(
				options['btn-loading']);
	}

	function form_success($form) {
		$form.find('[type=submit]').addClass('success').html(
				options['btn-success']);
		$form.find('.login-form-main-message').addClass('show success').html(
				options['msg-success']);
	}

	function form_failed($form) {
		$form.find('[type=submit]').addClass('error')
				.html(options['btn-error']);
		$form.find('.login-form-main-message').addClass('show error').html(
				options['msg-error']);
	}
})(jQuery);